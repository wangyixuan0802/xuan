import Vue from 'vue'
import Router from 'vue-router'
import Myh from '../components/myh.vue'
import ly from '../AGrassInHuozhou/3.vue'
import wzh from '../components/html4.vue'
import Hx from '../components/hx.vue'
Vue.use(Router)

export default new Router({
    routes: [
        {//此处为开始显示的首页
            path: '/',
            redirect: '/Hx'
        },
        {
            path: "/hx",
            component: Hx
        },
        {
            path: '/ly',
            component: ly
        },
        {
            path: '/wzh',
            component: wzh
        },
        {
            path: '/myh',
            component: Myh
        },
    ]
})
